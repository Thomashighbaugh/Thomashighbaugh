<div width="100%">
<img width="850px" height="300px" src="https://raw.githubusercontent.com/Thomashighbaugh/Thomashighbaugh/master/banner.svg" />

  <br />
  <hr />
  <br />
<details>
<summary> <b>Would you like to know more?</b></summary>
<br/>

<a href="https://thomasleonhighbaugh.me"> <img width="200px" src="./buttons/portfolio-button.svg" alt="Portfolio"/> </a>
<a href="https://resume-thomas-leon-highbaugh.vercel.app/" > <img width="200px" src="./buttons/resume.svg" alt="Resume"> </a>
<a href="https://biolink-delta.vercel.app"> <img width="200px" float="right" src="./buttons/contact_button.svg" alt="contact button"/></a>
  <br />
  <hr />
  <br />

  <details> <summmary><b>About This Image</b></summmary>
        <h3>About the Image Itself</h3>
        <p>The image is meant to simulate the menu of early digital cable television from the mid to late 1990s I remember as a kid. </p>
        <p>Using the style of these throwback menus gives me a good excuse for some glitch animations, which are especially fitting in this context.</p>
        <h3>Underlying Technical Functionality</h3>
        <p>This SVG image is actually written essentially as a typical webpage, with HTML5 elements, an internal style sheet, inline styles and internal Javascript functionality.</p>
        <p>Because GitHub's Markdown rendering engine will render SVGs, I am essentially able to change the "page's" extension to svg, embed it in an image tag and it will render on GitHub animations and all. </p>
        <p>The buttons at the bottom even work as clickable links, however you first would have to click the image and be taken to just the raw image then click the button again to see the link, thus I also provided the same links in matching buttons in the dropdown above as well.</p>
        <p><b>Note:</b> Getting the arrow keys to select menu items is still on the to-do list. As is better arranging and cleaning up the SVG's code. </p>
        <br />
        <hr />
        <br />

</details>
<details>
    <summary><b>Account Stats</b></summary>
      <img src="https://raw.githubusercontent.com/Thomashighbaugh/github-stats/master/generated/overview.svg#gh-dark-mode-only" alt="user stats"/>
    <img src="https://raw.githubusercontent.com/Thomashighbaugh/github-stats/master/generated/languages.svg#gh-dark-mode-only" alt="user stats" />
    <br />
    <hr />
    <br />
  </details>

  </details>
  
